import { Cocktail } from './../interfaces/cocktail.interface';
import { CocktailService } from './../../services/cocktail.service';
// import { Filtro } from './../interfaces/filtro.interface';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-cocktails',
  templateUrl: './list-cocktails.component.html',
  styleUrls: ['./list-cocktails.component.css']
})
export class ListCocktailsComponent implements OnInit {

  public showFilter: boolean;
  public loadCocktails: boolean; //spinner de carga

  public items: number;
  public page: number;

  listCocktails: Cocktail[] = [];
  searchBy = 'name';
  value = '';

  constructor( private cocktailService: CocktailService) {
    this.showFilter = false;
    this.loadCocktails = true;

    this.items = 12;
    this.page = 1;
  }

  ngOnInit() {
  }

  hideShowFilter() {
    this.showFilter = !this.showFilter;
  }


  filterData(){
    this.loadCocktails = false;
    this.cocktailService.getCocktailsFilter(this.searchBy, this.value).subscribe(cocktails => {
      console.log(typeof cocktails);
      this.listCocktails = cocktails;
        this.loadCocktails = true;

        console.log('medida',this.listCocktails.length)    
    })
  }

}
