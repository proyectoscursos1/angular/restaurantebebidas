import { Observable } from 'rxjs';
import { CocktailService } from './../../../services/cocktail.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Cocktail } from '../../interfaces/cocktail.interface';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-detail-cocktail',
  templateUrl: './detail-cocktail.component.html',
  styleUrls: ['./detail-cocktail.component.css']
})
export class DetailCocktailComponent implements OnInit {

  loadCocktail: boolean;
  
  constructor(private cocktailService: CocktailService, 
    private route: ActivatedRoute) {
      // this.cocktail = null;
      this.loadCocktail = true;      
    }

    id!: string;
    cocktail$!: Observable<Cocktail>;
      

  ngOnInit() {

    this.id = this.route.snapshot.paramMap.get('id') || '';
    this.cocktail$ = this.cocktailService.getCocktailById(this.id).pipe(tap());
    this.loadCocktail = true;
    


    // this.route.params.subscribe(params => {
    //   let id = params['id'];
    //   this.cocktailService.getCocktailById(id).subscribe(c => {
    //     this.cocktail = c;
    //     this.loadCocktail = true;
    //   })
    // })
  }

}
