import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Cocktail } from '../components/interfaces/cocktail.interface';

@Injectable({
  providedIn: 'root'
})
export class CocktailService {

  UrlId = "https://www.thecocktaildb.com/api/json/v1/1/lookup.php?";

  constructor(private http: HttpClient) { }

  getCocktailsFilter(searchBy: string, value: string) {

    const urlBase = "https://www.thecocktaildb.com/api/json/v1/1/";
    let additionalUrl = '';

    if (searchBy === 'name') {
      additionalUrl = 'search.php?s=' + value;
    } else {
      additionalUrl = 'filter.php?';

      if (searchBy === 'glass') {
        additionalUrl += 'g=';
      } else if (searchBy === 'category') {
        additionalUrl += 'c=';
      } else {
        additionalUrl += 'i=';
      }
      additionalUrl += value;
    }

    const finalURL = urlBase + additionalUrl;

    return this.http.get<Cocktail[]>(finalURL).pipe(map((res: any) => res.drinks));
  }


  // getCocktailById(id: string){
  //   return this.http.get("http://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=" + id).pipe(map((res: any) => res.drinks[0]));
  // }

  getCocktailById(i: string){
    const params = { i };
    return this.http.get(this.UrlId, { params }).pipe(map((res: any) => res.drinks[0]));
  }
  

}
